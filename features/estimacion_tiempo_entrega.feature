# language: es

@ete
Característica: Estimacion de tiempo de entrega

  """
  Cuando se hace un pedido se registra la hora en que se realizó
  Luego cuando el pedido es entregado se registra la hora entrega. 
  Ya con esto, se puede calcular cuanto tardó el envio.

  a - Cada tipo de menu tiene un tiempo particular de preparación, diria 10 min, 15. min y 20 min.
  b - Luego hay un tiempo de entrega que podríamos pensar en 10 min promedio.
  c - Si llueve el tiempo de entrega se incrementa en 5 min
  
  Para calcular el tiempo estimado se envio se aplica la siguiente heurística:
    Se promedia el tiempo que llevaron los últimos tres envios para pedidos del mismo menu
    Si no hay 3 pedidos del mismo tipo se toma el tiempo resultante de la suma de los a,b,c puntos anteriores
  """
Antecedentes:
  Dado el cliente "jperez"
  Y se registra con domicilio "Cucha Cucha 1234 1 Piso B" y telefono "4123-4123"
  Dado el cliente "carlos"
  Y se registra con domicilio "mars 1234" y telefono "4455-6677"
  Y que el repartidor "juanmotoneta" esta registrado

@estimado
Escenario: CT1 - Tiempo estimado para menu_individual sin lluvia ni pedidos previos
  Dado el cliente "jperez" pide un "menu_individual"
  Y no llueve
  Entonces el tiempo estimado de entrega es "20" minutos

@estimado
Escenario: CT2 - Tiempo estimado para menu_individual con lluvia y ni pedidos previos
  Dado el cliente "jperez" pide un "menu_individual"
  Y llueve
  Entonces el tiempo estimado de entrega es "25" minutos

@estimado
Escenario: CT3 - Tiempo estimado para menu_individual promediando pedidos anteriores
  Dado son las "00:00" el "02/12/2019"
  Y el cliente "jperez" pide un "menu_individual"
  Y son las "00:30" el "02/12/2019"
  Y es entregado
  Y son las "00:30" el "02/12/2019"
  Y el cliente "carlos" pide un "menu_individual"
  Y son las "00:50" el "02/12/2019"
  Y es entregado
  Y son las "01:00" el "02/12/2019"
  Y el cliente "carlos" pide un "menu_individual"
  Y son las "01:25" el "02/12/2019"
  Y es entregado
  Cuando son las "02:00" el "02/12/2019"
  Y el cliente "jperez" pide un "menu_individual"
  Entonces el tiempo estimado de entrega es "25" minutos