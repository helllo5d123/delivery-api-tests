# Delivery LaNona

Una casa de delivery ofrece 3 menus a sus clientes: Menu individual ($100), Menu parejas ($175), Menu familiar ($250).
La Nona quiere modernizarse y ofrecerle a sus clientes la posibilidad de pedir y gestionar los pedidos por un bot de Telegram. 
Para eso el bot debe permitir a los clientes registrarse con su domicilio y telefono, hacer un pedido, consultar el estado de un pedido, cancelar un pedido, ver el historico de pedidos, calificar una entrega (de 1 a 5).

El estado de los pedidos puede ser: Recibido, En preparación, En Camino, Entregado, Cancelado.

Solo pueden cancelarse los pedidos Recibidos y En preparación, y solo pueden calificarse los pedidos entregados.

Ademas de las APIs para Telegram se deben desarrollar otras APIs para la administración interna. 
Se desea poder registrar un repartidor con sus datos personales. 
Cambiar el estado de un pedido, teniendo en cuenta que al cambiar al estado de En Camino se debe asignar automaticamente un repartidor. 
La asignación se realiza de la siguiente forma: cada repartidor tiene un bolso en el que entran o bien 3 menues individuales, o 1 individual +  1 de pareja, o solo 1 familiar. 
El pedido se debe asignar al repartidor que tiene el bolso mas cercano a estar completo. 
Si se encuentra mas de un repartidor con este metodo, entonces de esos repartidores se debe elegir el que menos pedidos entrego en el día.

Una vez que el pedido pasa a estado entregado, el bolso se vacia y el repartidor esta listo para hacer una nueva entrega

Finalmente también se desea calcular las comisiones para los repartidores. 

Las comisiones son de 5% del valor de cada pedido entregado, pero si la entrega tuvo una calificación de 1, 
entonces la comisión es de 3% y si fue de 5 la calificación, la comisión es del 7%. 
A su vez los dias de lluvia las comisiones se incrementan en un 1% (*)

## Requerimiento de seguridad

* El endpoint de reset no debe estar disponible en el ambiente productivo
* La API tiene que recibir un encabezado api-key, si dicho encabezo no es enviado o su valor no coincide con el esperado la se debe devolver un error (status code 403)

## Casos de prueba aceptación

Los casos que constituyen el MVP son: 
(@mvp) RC1, RC2, RC3, RC4, P1, P2, CP1, CP2, CAP1, CAP2, CAP3, CAP4, RP1, RP2, RP3, COM1, COM2, COM3, COM4, COM5, COM6, CEP1, CEP2, CEP3, CEP4, A1, A2, A3, ERR1, TELE1, TELE2, TELE3, TELE4, TELE5, TELE6, TELE7, TELE8, TELE9, TELE10, TELE11

Los casos complementarios son:
* Limte de 10 minutos en la espera del repartidor (@aa AA1) 
* Cancalación de pedido (@cancelacion CANP1,CANP2, CANP3, CANP4, CANP4)
* Consulta historica (@historica CH1, CH2, CH3)
* Consulta de tiempo estimado (@estimado CT1, CT2, CT3)
* Otras validaciones adicionales al MVP (@mvpex)

## Convenciones de implementacion de la API

* Todos los request que resulten en errores de validación de reglas de negocio debe devolver un status code 400



## Correr tests con el app contra localhost:4567

```
$ API_KEY=zarzarza bundle exec rake features
```

## Correr tests con el app contra una API remota

```
$ API_KEY=zarzarza BASE_URL=http://myweb.com bundle exec rake features

```

## Algunas herramientas

* https://requestbin.com
* http://wiremock.org
* https://openweathermap.org/api